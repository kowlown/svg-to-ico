package com.mystnihon.svgico.util;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageComponent {
    private final MessageSource source;

    public MessageComponent() {
        source = messageSource();
    }

    public String getString(String key, Object... args) {
        return source.getMessage(key, args, Locale.getDefault());
    }

    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:" + ResourceConstantName.RESOURCE_BUNDLE);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}
