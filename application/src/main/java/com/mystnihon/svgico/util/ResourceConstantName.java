package com.mystnihon.svgico.util;

public class ResourceConstantName {

    public static final String RESOURCE_BUNDLE = "strings";

    private ResourceConstantName() {

    }
}
