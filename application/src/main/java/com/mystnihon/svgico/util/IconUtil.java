package com.mystnihon.svgico.util;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import net.sf.image4j.codec.ico.ICODecoder;

import javax.swing.JFrame;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class IconUtil {

    private IconUtil() {

    }

    public static void setIcon(JFrame frame) {
        IconUtil.getIcons().ifPresent(frame::setIconImages);
    }


    public static void setIcon(Dialog<?> dialog) {
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        IconUtil.setIcon(stage);
    }

    public static void setIcon(Stage stage) {
        IconUtil.getIcons()
            .map(bufferedImages -> bufferedImages.stream()
                .map(bufferedImage -> SwingFXUtils.toFXImage(bufferedImage, null))
                .collect(Collectors.toList()))
            .ifPresent(writableImages -> stage.getIcons().addAll(writableImages));
    }

    private static Optional<List<BufferedImage>> getIcons() {
        try {
            return Optional.of(ICODecoder.read(IconUtil.class.getClassLoader().getResourceAsStream("images/weather_app.ico")));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
