package com.mystnihon.svgico.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;

@SuppressWarnings("unused")
@Slf4j
@Component
public class PropertyComponent {
    public static final String FILE_PROPERTIES = "properties.prop";
    public static final String COMMENTS = "This is a comments";
    private Properties properties;

    public Optional<File> getLastFileFor(String key) {
        open(false);
        return Optional.ofNullable(properties.getProperty(key)).map(File::new);
    }

    public void setLastFileFor(String keyImage, File file) {
        open(true);
        properties.put(keyImage, file.getAbsolutePath());
        write();

    }

    public Optional<String> getValueFor(String key) {
        open(false);
        return Optional.ofNullable(properties.getProperty(key));
    }

    public <U> Optional<U> getValueFor(String key, Function<String, U> converter) {
        open(false);
        return Optional.ofNullable(properties.getProperty(key)).map(converter);
    }

    public <T> void setValueFor(String key, T value) {
        open(true);
        properties.put(key, String.valueOf(value));
        write();
    }

    @PostConstruct
    public void init() {
        open(false);
    }

    @PreDestroy
    public void terminate() {
        write();
    }

    public void open(boolean force) {
        if (properties == null || force) {
            properties = new Properties();
            try (FileInputStream fileInputStream = new FileInputStream(PropertyComponent.FILE_PROPERTIES)) {
                properties.load(fileInputStream);
            } catch (IOException e) {
                log.error("Reading properties", e);
            }
        }
    }

    private void write() {
        try (FileOutputStream fileOutputStream = new FileOutputStream(PropertyComponent.FILE_PROPERTIES)) {
            properties.store(fileOutputStream, PropertyComponent.COMMENTS);
        } catch (IOException e) {
            log.error("IO read/write properties", e);
        }
    }
}
