package com.mystnihon.svgico.util;

public class PropertyKeys {
    public static final String KEY_IMAGE_SVG = "svg.last.directory";
    public static final String KEY_IMAGE_ICO = "ico.last.directory";

    private PropertyKeys() {
    }
}
