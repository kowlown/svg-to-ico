package com.mystnihon.svgico.util;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableBooleanValue;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;

public class BooleanSupplierProperty implements ObservableBooleanValue {

    private final BooleanSupplier booleanSupplier;
    private final List<ChangeListener<? super Boolean>> changeListeners;
    private final List<InvalidationListener> invalidationListeners;

    public BooleanSupplierProperty(BooleanSupplier booleanSupplier) {

        this.booleanSupplier = booleanSupplier;
        changeListeners = new ArrayList<>();
        invalidationListeners = new ArrayList<>();
    }

    @Override
    public boolean get() {
        return booleanSupplier.getAsBoolean();
    }

    @Override
    public void addListener(ChangeListener<? super Boolean> listener) {
        changeListeners.add(listener);
    }

    @Override
    public void removeListener(ChangeListener<? super Boolean> listener) {
        changeListeners.remove(listener);
    }

    @Override
    public Boolean getValue() {
        return booleanSupplier.getAsBoolean();
    }

    @Override
    public void addListener(InvalidationListener listener) {
        invalidationListeners.add(listener);

    }

    @Override
    public void removeListener(InvalidationListener listener) {
        invalidationListeners.remove(listener);
    }

    public void publish() {
        for (InvalidationListener invalidationListener : invalidationListeners) {
            invalidationListener.invalidated(this);
        }
        for (ChangeListener<? super Boolean> changeListener : changeListeners) {
            changeListener.changed(this, !get(), get());
        }
    }
}
