package com.mystnihon.svgico;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static javafx.application.Application.launch;

@SpringBootApplication
public class SvgIcoApplication {

    public static void main(String[] args) {
        launch(JavaFxApplication.class, args);
    }
}
