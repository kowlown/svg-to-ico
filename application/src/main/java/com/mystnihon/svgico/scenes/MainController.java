package com.mystnihon.svgico.scenes;

import com.mystnihon.svgico.converter.SvgToRasterizeImageConverter;
import com.mystnihon.svgico.exceptions.AsyncException;
import com.mystnihon.svgico.util.BooleanSupplierProperty;
import com.mystnihon.svgico.util.MessageComponent;
import com.mystnihon.svgico.util.PropertyComponent;
import com.mystnihon.svgico.util.PropertyKeys;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;
import net.sf.image4j.codec.ico.ICOEncoder;
import org.apache.batik.transcoder.TranscoderException;
import org.springframework.stereotype.Controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
@Controller
public class MainController implements Initializable {

    private static final int DELAY_BEFORE_LABEL_CLEANING = 10;
    private final SvgToRasterizeImageConverter svgToRasterizeImageConverter;
    private final PropertyComponent propertyComponent;
    private final MessageComponent messageComponent;
    private final ScheduledExecutorService executor;
    @FXML
    protected CheckBox a16pxCheckBox;
    @FXML
    protected CheckBox a32pxCheckBox;
    @FXML
    protected CheckBox a64pxCheckBox;
    @FXML
    protected CheckBox a128pxCheckBox;
    @FXML
    protected CheckBox a256pxCheckBox;
    @FXML
    protected CheckBox a512pxCheckBox;
    @FXML
    protected TextField svgPathTextField;
    @FXML
    protected Label labelStatus;
    @FXML
    protected Button buttonValidate;
    @FXML
    protected ProgressBar progressBar;
    @FXML
    protected ImageView imageViewPreview;

    private File selectedFile;
    private ScheduledFuture<?> scheduledFuture;
    final BooleanSupplierProperty fileSelectedProperty;

    public MainController(SvgToRasterizeImageConverter svgToRasterizeImageConverter, PropertyComponent propertyComponent,
        MessageComponent messageComponent) {
        this.svgToRasterizeImageConverter = svgToRasterizeImageConverter;
        this.propertyComponent = propertyComponent;
        this.messageComponent = messageComponent;
        executor = Executors.newScheduledThreadPool(10);
        fileSelectedProperty = new BooleanSupplierProperty(() -> selectedFile != null);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imageViewPreview.managedProperty().bind(imageViewPreview.visibleProperty());
        BooleanBinding checkboxesProperties = a16pxCheckBox.selectedProperty().or(a32pxCheckBox.selectedProperty())
            .or(a64pxCheckBox.selectedProperty()).or(a128pxCheckBox.selectedProperty()).or(a256pxCheckBox.selectedProperty())
            .or(a512pxCheckBox.selectedProperty());

        BooleanBinding booleanBinding = checkboxesProperties.and(fileSelectedProperty);

        buttonValidate.disableProperty().bind(booleanBinding.not());
    }

    @FXML
    protected void handlePickSvg(ActionEvent ignoredActionEvent) {
        FileChooser fileChooser = new FileChooser();
        propertyComponent.getLastFileFor(PropertyKeys.KEY_IMAGE_SVG).ifPresent(fileChooser::setInitialDirectory);
        fileChooser.setTitle(messageComponent.getString("message.file.chooser.choose.image.file.title"));
        fileChooser.getExtensionFilters()
            .addAll(new FileChooser.ExtensionFilter(messageComponent.getString("message.file.chooser.filter.images.svg"), "*.svg"),
                new FileChooser.ExtensionFilter(messageComponent.getString("message.file.chooser.filter.all"), "*.*"));
        Optional.ofNullable(fileChooser.showOpenDialog(buttonValidate.getScene().getWindow())).ifPresent(this::openSelectedFileFromFileChooser);
    }

    @FXML
    protected void handleValidate(ActionEvent ignoredActionEvent) {
        Optional.ofNullable(selectedFile).ifPresent(file -> {
            try {
                transcodeFile(file);
            } catch (TranscoderException | IOException ex) {
                MainController.log.error("Transcoding error", ex);
                displayOnStatus(messageComponent.getString("label.transcoding.error"));
            }
        });
    }

    private void openSelectedFileFromFileChooser(File selectedFile) {
        propertyComponent.setLastFileFor(PropertyKeys.KEY_IMAGE_SVG, selectedFile.getParentFile());
        this.selectedFile = selectedFile;
        fileSelectedProperty.publish();
        svgPathTextField.setText(selectedFile.getAbsolutePath());
        try {
            displayPreview(selectedFile);
        } catch (TranscoderException | IOException e) {
            log.error("Error", e);
        }
    }

    private void displayPreview(File selectedFile) throws TranscoderException, IOException {
        imageViewPreview.setVisible(true);
        imageViewPreview.setImage(SwingFXUtils.toFXImage(createBufferedImageFromFile(selectedFile, imageViewPreview.getFitHeight()), null));
    }

    private void transcodeFile(File selectedFile) throws TranscoderException, IOException {
        List<BufferedImage> bufferedImages = new ArrayList<>();
        addBufferImageForCase(selectedFile, bufferedImages, a16pxCheckBox, 16);
        addBufferImageForCase(selectedFile, bufferedImages, a32pxCheckBox, 32);
        addBufferImageForCase(selectedFile, bufferedImages, a64pxCheckBox, 64);
        addBufferImageForCase(selectedFile, bufferedImages, a128pxCheckBox, 128);
        addBufferImageForCase(selectedFile, bufferedImages, a256pxCheckBox, 256);
        addBufferImageForCase(selectedFile, bufferedImages, a512pxCheckBox, 512);

        if (bufferedImages.isEmpty()) {
            log.warn("Nothing is selected");
            displayOnStatus(messageComponent.getString("label.nothing.selected"));
        } else {
            FileChooser fileChooser = new FileChooser();
            propertyComponent.getLastFileFor(PropertyKeys.KEY_IMAGE_ICO).ifPresent(fileChooser::setInitialDirectory);
            fileChooser.setTitle(messageComponent.getString("message.file.chooser.choose.directory.title"));
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(messageComponent.getString("message.file.chooser.filter.images.ico"), "*.ico"),
                new FileChooser.ExtensionFilter(messageComponent.getString("message.file.chooser.filter.all"), "*.*"));
            Optional.ofNullable(fileChooser.showSaveDialog(buttonValidate.getScene().getWindow())).ifPresent(file -> proceedToSaveIcoFile(bufferedImages, file));
        }
    }

    private void proceedToSaveIcoFile(List<BufferedImage> bufferedImages, File file) {
        propertyComponent.setLastFileFor(PropertyKeys.KEY_IMAGE_ICO, file.getParentFile());
        progressBar.setVisible(true);
        CompletableFuture.runAsync(() -> writeIcoFile(bufferedImages, file)).thenAccept(d -> Platform.runLater(() -> {
            progressBar.setVisible(false);
            displayOnStatus(messageComponent.getString("label.file.created", file.getName()));
        })).exceptionally(throwable -> {
            Platform.runLater(() -> {
                progressBar.setVisible(false);
                displayOnStatus(messageComponent.getString("label.file.creation.error", file.getName()));
            });
            return null;
        });
    }

    private void displayOnStatus(String text) {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
        labelStatus.setText(text);
        scheduledFuture = executor.schedule(() -> Platform.runLater(() -> labelStatus.setText("")), DELAY_BEFORE_LABEL_CLEANING, TimeUnit.SECONDS);

    }

    private void writeIcoFile(List<BufferedImage> bufferedImages, File file) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            ICOEncoder.write(bufferedImages, fileOutputStream);
        } catch (IOException e) {
            MainController.log.error("Write error", e);
            throw new AsyncException(e);
        }
    }

    private void addBufferImageForCase(File selectedFile, List<BufferedImage> bufferedImages, CheckBox checkBoxSelected, int iconSize)
        throws TranscoderException, IOException {
        if (checkBoxSelected.isSelected()) {
            bufferedImages.add(createBufferedImageFromFile(selectedFile, iconSize));
        }
    }

    private BufferedImage createBufferedImageFromFile(File selectedFile, double imageSize) throws TranscoderException, IOException {
        return svgToRasterizeImageConverter.transcodeSVGToBufferedImage(selectedFile, imageSize, imageSize);
    }
}
